class RemoveMessageToUsers < ActiveRecord::Migration[5.1]
  def change
    remove_reference :users, :message, foreign_key: true
  end
end
