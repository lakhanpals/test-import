class AddTitleToOrderItem < ActiveRecord::Migration[5.1]
  def change
    add_column :order_items, :title, :string
  end
end
