class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :title, null:false
      t.text :description
      t.string :image
      t.float :price,null: false

      t.timestamps
    end
  end
end
