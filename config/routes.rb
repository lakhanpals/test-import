Rails.application.routes.draw do

  root 'pages#home', as: 'home'

  get 'users/new'

  get '/new'=> 'users#new' 
  post '/new'=> 'users#create'
  get 'sessions/new' => 'sessions#new'
  post 'sessions/new' => 'sessions#create'
  get 'sessions' => 'sessions#new'
  post 'dashboard' => 'admin#dashboard'
  resources :items
  resource :sessions, only: [:new, :create, :destroy]
  resources :pages, only: [:menu] 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'showmsg' => 'messages#showmsg'
  get 'about' => 'pages#about', as: 'about'

  get 'login'=> 'pages#login', as: 'login'
  delete 'logout' => 'sessions#destroy'
  delete 'showmsg' => 'messages#destroy'
  post 'login' => 'sessions#create'
  get 'menu'=> 'pages#menu', as: 'menu'

  get 'contact'=> 'pages#contact', as: 'contact'
  post 'contact' => 'contact#create'
  get 'tocart/:id'=>'pages#tocart', as:'tocart'
  get '/cart', to: 'order_items#index'
  post '/tocart/:id', to: 'order_items#index'
  resources :order_items
  post 'order_items/:id/add' => 'order_items#add_quantity', as: 'order_item_add'
  post 'order_items/:id/reduce' => 'order_items#reduce_quantity', as: 'order_item_reduce'
 get 'cart/:id/edit' => 'pages#edit'
  get 'login_admin' => 'admin#login_admin', as: 'login_admin'
 get 'dashboard' => 'admin#dashboard'
  get 'index' => 'items#index', as: 'index' 
end
