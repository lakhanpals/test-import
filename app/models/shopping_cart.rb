class ShoppingCart

  delegate :sub_total, to: :order

  def initialize(token:)
    @token = token
    update_sub_total
  end

  def order
    @order ||= Order.find_or_create_by(token: @token) do |order|
      order.sub_total = 0
    end
  end

  def order_item
    @order_item = order.items.find_or_initialize_by(
      item_id: item_id
    )
  end

  def items_count
    order.items.sum(:quantity)
  end

  def add_item(item_id:, quantity: 1, title:)
    product = Item.find(item_id)

    order_item = order.items.find_or_initialize_by(
      item_id: item_id
    )

    order_item.price = product.price
    order_item.quantity = quantity
    order_item.title = product.title
    ActiveRecord::Base.transaction do
    order_item.save
    update_sub_total
    end
  end

  def remove_item(id:)
    ActiveRecord::Base.transaction do
    order.items.destroy(id)
    update_sub_total
   end
  end

  private

  def update_sub_total
    # puts 'test sub total'
    order.sub_total = order.items.sum('quantity * price')
    order.save
  end


end
