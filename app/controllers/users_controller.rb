class UsersController < ApplicationController
#	before_action :set_user, only: [:show, :destroy]
 def new
  	@user = User.new
  end

  def create
  	@user = User.new(user_params)
  	if @user.save
  		redirect_to login_path, :notice=> 'User was successfully created'
  	else
  		redirect_to new_path, :notice=> 'Account not created. Try again'
  end
end
  def show
  	@user = User.find(params[:id])
	end
private
	 def user_params
    params.require(:user).permit( :username, :email, :password, :password_confirmation)
  end

end


