class SessionsController < ApplicationController
	
  def new
  end
def create
  user = User.authenticate(params[:session][:email],params[:session][:password])
	#session[:user_id] =user.id
	if user.nil?
		logger.debug
  	flash.now[:danger] = "Invalid Login/password combination"
  	redirect_to login_path, :notice =>"Invalid Login/password combination"
  else
	  sign_in user
	  flash[:success] = "Signed In"
	  #redirect_to home_path
	  return redirect_to(home_path) if current_user.present?

	end
end
def destroy
	sign_out
	redirect_to home_path, :notice =>"Logged Out"
end
end

# private
# def login_params
# 	params.fetch[:user].permit[:email,:password]
# end

