class OrderItemsController < ApplicationController

	def index
    	@curr_cart_items = current_cart.order.items
  	end

    def create
        current_cart.add_item(
      		item_id: params[:item_id],
      		quantity: params[:quantity],
          title: params[:title]
    	)
         	redirect_to cart_path
	end
   def update
    if current_cart.update_item(id: params[:id])
      redirect_to tocart_path
  end
end
	def destroy
    current_cart.remove_item(id: params[:id])
    redirect_to cart_path
  end

  def add_quantity
    current_cart.increase_quantity(id: params[:id])

    redirect_to cart_path 
  end

  def add_quantity
    @curr_cart_item = OrderItem.find(params[:id]) 
    @curr_cart_item.quantity +=1
    @curr_cart_item.save
    
    redirect_to cart_path 
  end
  
   def reduce_quantity
    @curr_cart_item = OrderItem.find(params[:id]) 
    if @curr_cart_item.quantity > 1 
      @curr_cart_item.quantity -=1
    end
    @curr_cart_item.save
    # update_sub_total
    redirect_to cart_path 
  end

   def order_item_params
      params.require(:order_item).permit(:item, :quantity, :title, :order_id, :item_id )
    end 
end
