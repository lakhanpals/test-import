class MessagesController < ApplicationController
   before_action :set_message, only: [:show, :edit, :update, :destroy]

	def index
     @messages = Message.all
   end
   def showmsg
      @message = Message.all
    end
            
   def new
      @message = Message.new        
   end
            
    def destroy
      @message = Message.find(params[:id])
      if @message.present?
      @message.destroy
      respond_to do |format|
      format.html { redirect_to showmsg_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
    end
     end

      def set_message
      @message = Message.find(params[:id])
    end
end
